package xyz.stepsecret.joke2;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.HEAD;
import xyz.stepsecret.joke2.API.GET_Joke;
import xyz.stepsecret.joke2.Model.Get_Joke_Model;
import xyz.stepsecret.joke2.TinyDB.TinyDB;

public class MainActivity extends AppCompatActivity {

    private TextView head;
    private TextView body;

    private ScrollView LY;

    private String API = "https://stepsecret.xyz";

    public static RestAdapter restAdapter;

    private TinyDB tinydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();

        tinydb = new TinyDB(getApplicationContext());

        State.Save_Joke = tinydb.getInt("Save_Joke",0);

        head = (TextView) findViewById(R.id.head);
        body = (TextView) findViewById(R.id.body);

        LY = (ScrollView) findViewById(R.id.scrollView);


        head.setSelected(true);

       // head.setText("รหัสลับ");
        //body.setText("หลังจากที่คุณพ่อทราบว่า “ป๋อง” ลูกชายวัยรุ่นชอบแอบเปิดอินเทอร์เน็ต เข้าไปดูเว็บไซต์โป๊ๆ เป็นประจำ คุณพ่อจึงตั้ง password ไว้ เพื่อป้องกันมิให้ลูกชายตัวดีแอบมาเปิดใช้เครื่องคอมพิวเตอร์อีกต่อไป เรื่องอะไรจะยอมจนมุมง่ายๆ “ป๋อง” จึงได้ว่าจ้างน้องเก๋ น้องสาววัย ๘ ขวบ ของตนด้วยขนมถุงใหญ่ให้แอบไปชะโงกดูคุณพ่อตอนเปิดเครื่อง คอมพิวเตอร์ว่าคุณพ่อพิมพ์ password ตัวอะไรลงไปบ้าง น้องเก๋ยินดีรับงานด้วยความเต็มใจ วันหนึ่งขณะที่คุณพ่อกำลังจะเปิด เครื่องคอมพิวเตอร์ น้องเก๋ก็แกล้งทำเป็นมานั่งข้างๆ คุณพ่อ ในขณะเดียว กันก็แอบชำเลืองดูคุณพ่อพิมพ์ password ไปด้วย ทันทีที่รู้รหัสลับ น้องเก๋ก็รีบวิ่งแจ้นออกมาหา “ป๋อง” พี่ชายที่ยืนรอด้วยใจ ระทึกอยู่นอกบ้าน ป๋อง : รู้หรือยังๆ เก๋ : รู้แล้ว ! คุณพ่อพิมพ์สี่ตัวจำได้แม่นเลย ป๋อง : มีตัวอะไรบ้าง บอกมาเร็ว! เก๋ : เอาขนมมาก่อนสิ ป๋องจึงยื่นขนมถุงใหญ่ให้น้องสาว น้องเก๋รีบคว้าถุงขนมทันที พร้อมกระซิบข้างหูบอกพี่ชายว่า เก๋ : จดไว้นะ คุณพ่อพิมพ์ว่า….. “ดอกจัน ดอกจัน ดอกจัน แล้วก็.. ดอกจัน ” ป๋อง : !!!??!!");

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Intent i = new Intent(getApplicationContext(), MainActivity.class);
                //startActivity(i);
                //finish();

                setJoke();

            }
        });


        if(!State.get_joke_success && isNetworkConnected())
        {
            call();

        }


        setJoke();



    }

    public void call()
    {
        final GET_Joke Get_Joke_API = restAdapter.create(GET_Joke.class);

        Get_Joke_API.Get_EV_API("Get_EV", new Callback<Get_Joke_Model>() {
            @Override
            public void success(Get_Joke_Model Joke_Model, Response response) {
                String[][] dataJoke = Joke_Model.dataJoke();
                ArrayList<String> Head = new ArrayList<String>();
                ArrayList<String> Body = new ArrayList<String>();

                for (int i = 0; i < dataJoke.length; i++) {

                    Head.add(dataJoke[i][0]);
                    Body.add(dataJoke[i][1]);


                }

                tinydb.putListString("Head", Head);
                tinydb.putListString("Body", Body);

                Log.e("LOG TAG", "Get_Joke success");

            }

            @Override
            public void failure(RetrofitError error) {


                Log.e("LOG TAG", "Get_Joke failure");

                call();

            }
        });
    }

    public void setJoke()
    {
        ArrayList<String> GetHead = new ArrayList<String>();
        ArrayList<String> GetBody = new ArrayList<String>();

        GetHead = tinydb.getListString("Head");
        GetBody = tinydb.getListString("Body");

        if(GetHead.size() > 0)
        {
            if(GetHead.size() < State.Save_Joke+1)
            {
                State.Save_Joke = 0;
                head.setText("" + GetHead.get(State.Save_Joke));
                body.setText("" + GetBody.get(State.Save_Joke));
                State.Save_Joke++;
                tinydb.putInt("Save_Joke", State.Save_Joke);

            }
            else
            {

                head.setText(""+GetHead.get(State.Save_Joke));
                body.setText(""+GetBody.get(State.Save_Joke));
                State.Save_Joke++;
                tinydb.putInt("Save_Joke",State.Save_Joke);
            }
        }


        int random = (int )(Math.random() * 0xFFFFFF + 0xFF000000);
        LY.setBackgroundColor(random);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
