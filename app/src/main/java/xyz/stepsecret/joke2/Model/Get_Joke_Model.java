package xyz.stepsecret.joke2.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stepsecret on 21/2/2559.
 */
public class Get_Joke_Model {

    @SerializedName("data")
    private String[][] dataJoke ;

    public String[][] dataJoke() {
        return dataJoke;
    }
}
