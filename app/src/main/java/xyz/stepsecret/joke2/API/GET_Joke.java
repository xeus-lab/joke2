package xyz.stepsecret.joke2.API;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import xyz.stepsecret.joke2.Model.Get_Joke_Model;

/**
 * Created by stepsecret on 21/2/2559.
 */
public interface GET_Joke {

    @GET("/Joke")
    public void Get_EV_API(@Query("type") String type, Callback<Get_Joke_Model> response);
}
